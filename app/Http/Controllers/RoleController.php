<?php

namespace App\Http\Controllers;

use Validator;
use App\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page'] = [
            'title' => 'List of Roles'
        ];
        $data['roles'] = DB::table('roles')->paginate(1);
        return view('roles.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page'] = [
            'title' => 'Create Role'
        ];
        return view('roles.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:roles,name',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->action('RoleController@create')->withInput()->withErrors($validator);
        }
        $role = new Role;

        $role->name = $request->name;
        $role->description = $request->description;
        $role->save();
        
        return redirect()->action('RoleController@create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['page'] = [
            'title' => 'Show Role'
        ];
        $data['role'] = Role::find($id);
        return view('roles.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page'] = [
            'title' => 'Edit Role'
        ];
        $data['role'] = Role::find($id);
        return view('roles.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:roles,name,'.$id,
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->action('RoleController@edit', [
                'role' => $id
            ])->withInput()->withErrors($validator);
        }
        $role = Role::find($id);

        $role->name = $request->name;
        $role->description = $request->description;
        $role->save();
        return redirect()->action('RoleController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Role::destroy($id);
        return redirect()->action('RoleController@index');
    }
}
