@extends('main.index')

@section('content')
<script>
    function destroy(id){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        $.ajax({
            url:'/roles/'+1,
            type:"post",
            data: { _method:"DELETE" },
            success: function(data) {
                alert('delete success');
                $("#role" + id).remove();
            }
        });
    }
</script>
    <div class="box">
        <div class="box-body no-padding">
            <table class="table">
                <tbody>
                    <tr>
                        <th style="width: 10px">ID</th>
                        <th>Name</th>
                        <th>Description</th>
                    </tr>
                    @foreach ($roles as $role)
                        <tr id="role{{$role->id}}">
                            <td>{{$role->id}}</td>
                            <td>{{$role->name}}</td>
                            <td>{{$role->description}}</td>
                            <td>
                                <button onclick="window.location = '/roles/{{$role->id}}'" class="btn btn-primary btn-sm btn-detail">View</button>
                                <button onclick="window.location = '/roles/{{$role->id}}/edit'" class="btn btn-warning btn-sm btn-detail">Edit</button>
                                <button onclick="destroy({{$role->id}})" class="btn btn-danger btn-sm btn-delete">Delete</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="pull-right">
                {{ $roles->links() }}
            </div>
        </div>
    </div>
@endsection